\section{system design}
\label{design}

In this section, we discuss about the system design and provide insights from our implementation of the design. An overview of our design is shown in Figure \ref{fig:design}. ImageSuggest comprises two main components - a \textit{Ranker} and a \textit{Learner}.

% \includepdf{./images/system_design.png}

\begin{figure*}[!ht]
    \centering
    \includegraphics[width=\textwidth]{./images/system_design.png}
    \caption{Design of ImageSuggest}
    \label{fig:design}
\end{figure*}


\subsection{Ranker}
The Ranker component accepts text input from the user and outputs a list of images that best describe the input text. This component has three modules, viz - pre-processor, TextRanker and post-processor.

\subsubsection{Pre-Processor}
The text is first passed through the pre-processor module. In this module, stop words from the input text are removed and the text is transformed to lower case (case folding). In addition to the above, this module also performs stemming while keeping track of the original words. This way, we reduce the size of the generated graph in the TextRanker below to yield better results faster.

\subsubsection{TextRanker}
The pre-processed text is then fed to the TextRanker module which implements the TextRank algorithm \cite{textrank} to summarize the text. TextRank was chosen as it is an unsupervised text summarization algorithm which fits our needs to rapidly develop a summariser. The working of TextRank algorithm is strikingly similar to PageRank algorithm. Each word (unigrams) from the input text is represented as a node in the graph. Edges are created between nodes/tokens that fall within a fixed window in the text. This is identified as the \textit{CO-OCCURRENCE} window and is one of the essential parameters of the TextRank algorithm. The paper notes that a window of size between 7-10 to be optimal and we used a size of 7 in our runs. Once such a graph is constructed, the PageRank algorithm is run to identify the most prominent words that best summarize the text. 

Similar to TextRank which helps rank tokens/unigrams, the paper suggested using SentenceRank which runs along the same lines of developing a graph representation of the sentences in the text with each sentence forming a node. Instead of using the \textit{CO-OCCURRENCE} window to create edges, we use the sentence similarity to assign edge weights between nodes. The resulting ranking for sentences has been used as one of the features for the classifier as described below.

\subsubsubsection{Example of Graph constructed}
\label{example-textrank}
Consider the following text as input: ``A laptop is a portable personal computer with a clamshell form factor suitable for mobile use''. During pre-processing, stop words from the text are removed and the entire text is case folded. For the purposes of this illustration, we're not stemming the text. However, we should note that the implemented algorithm stems text. The graph constructed for the given input is shown in Figure \ref{fig:example}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=8cm]{./images/example_is.png}
    \caption{Example of TextRank - Refer to section \ref{example-textrank} for more details}
    \label{fig:example}
\end{figure}

In the illustration (Figure \ref{fig:example}) we chose to represent four edges from node ``laptop'' and three edges for other nodes. The rationale behind this was to keep the image free of clutter. In general, the co-occurrence window (number of edges from a node) is constant for the entire algorithm.

\subsubsubsection{Why TextRank works?}
Once the graph is constructed, PageRank algorithm is run to identify the most prominent words that best summarize the text. The intuition behind TextRank is that prominent words are repeated quite often in the text. These prominent words appear frequently alongside related words and hence they will have more edges into them. Thus, PageRank algorithm (on convergence) will give a higher score to such words. This is pretty similar to how humans in general scan a block of text and identify key points. 

\subsubsubsection{Advantages}
Using TextRank to summarise text has the following advantages:
\begin{itemize}
\item The system is entirely \textit{language-agnostic} Although the enhancements we added including the \textit{"ADJECTIVE\_INFLUENCE"} and the features used for the classifier would need linguistic knowledge to some extent, they do not form the core of the summariser and such knowledge is pretty trivial to obtain as well. 
\item The summariser \textit{requires minimal training} and does not suffer from any cold-start problems. The trained classifer only complements the results from the summariser.
\item The summariser also provides a rule-based model which is easier to fine-tune than complex machine-learning systems. For example, the \textit{"ADJECTIVE\_INFLUENCE"} we have included (explained later) was pretty straight-forward to implement and its direct effect on the summarisation allowed us to fine tune its weight rather quickly.
\end{itemize}

\subsubsection{Post-Processor}
The TextRank module returns most prominent words in the text and these are fed as input to the post-processing module. This module sorts the prominent words based on the PageRank score and generates bigrams and trigrams based on these unigrams' adjacency. We restricted ourselves to trigrams as we felt that a trigram would be sufficient to capture any important feature in the text.

\subsubsection{Image Search}
These n-gram summaries (unigrams, bigrams and trigrams) are fed as input to the next component, \textit{Image Search Component}, which uses Google Image Search API.  In addition to the image URLs, it also returns additional information about the webpage the image is hosted in. The results are provided to the Learner module.

\subsection{Learner}
The Learner module is primarily responsible for ranking the resulting images better via capturing important features of their corresponding n-gram summaries. The list of images from the Image Search module is fed as input to this module. Based on the images provided and the text summarized by the TextRanker, we extract appropriate features which are explained below. Most of these features are based on prior literature related to text summarisation(\cite{auto-ml} and \cite{train-summary})

\begin{center}
  \begin{tabular}{| l | p{4cm} |}
    \hline
    TEXT\_RANK & The value of TextRank score for the summarized words\\
    RELATIVE\_POS & Relative position of the n-gram in the original text\\
    LEN & Length of the n-gram \\  
    SIMILARITY & Cosine Similarity between the input text and the text rendered on the page hosting the matching image \\
    SENTENCE\_RANK & Value of textrank score taking sentences as nodes (instead of unigrams) \\
    CONTAINS\_ADJECTIVE & Does the summary have an adjective?\\
    CONTAINS\_PROPER\_NOUN & Does the summary have a proper noun?\\
    SEARCH\_RANK & Feedback provided by user - could be relevance (0/1) or a number. \\
    \hline
  \end{tabular}
\end{center}

The features are fed to an SVM classifier (with linear kernel) to be trained later using \textit{Learning To Rank} algorithm. Once the features are extracted, the images are shown to the user ordered based on the current classifier state. For each image shown to the user, the user can judge the image as relevant or irrelevant. To mark an image as relevant, the user is prompted to click the Accept button on the user interface. To mark an image as irrelevant, the user is prompted to click the Reject button on the user interface. 

These decisions are fed to the classifier (feedback loop). The classifier now has two inputs, the features and the judgement values for the features. Based on these two entities, the classifier tunes the features to identify the most prominent features for ranking images. As the features are trained, results for future queries will improve. As of now, we support only relevant/irrelevant decisions in the feedback mechanism.

\subsection{Lessons Learnt}
Overall, we found the TextRank module itself to be working very effectively. The summaries it provided returned relevant results from Google Image Search. To tune the system better towards Image search, we felt the we could tweak the edge weights to reflect Adjective properties better. For example, a text on "Fast trains" could have better image results if we clubbed the words "Fast" and "trains" together. So, we included the variable \textit{"ADJECTIVE\_INFLUENCE"} which  accounted for 30\% of the edge weight and the remaining edge weight was based on CO-OCCURENCE. Also, the edge weight was normalised by the distance between the tokens in the original text which too was not part of the original paper. This helped us reduce noisy associations in the text graph, especially for adjectives since an adjective has effect mostly on its closest neighbouring tokens only.

With reliable results from the TextRank module, we developed the Classifier to primarily eliminate or atleast de-prioritise outliers from the results. For instance, the example given in the screenshot shows a wiki-extract on Laptops wherein we could see a noisy summarisation from TextRank, namely \textit{clamshell form factor} The classifier could downgrade such terms once we get the related user feedback as its features of LEN, CONTAINS\_ADJECTIVE and SEARCH\_RANK would help the classifier learn on why it was downgraded. In this sense, we see the classifer to complement the working of TextRank as we get more training.