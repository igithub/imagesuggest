from TextRank import summary
import cjson
from os import path
from collections import defaultdict
import pickle
from uuid import uuid1
from pickle import HIGHEST_PROTOCOL

from utils import Search, count_adjective, count_proper_noun
from recommender.utils import Text_Similarity
import TextRank
from cloud.serialization import cloudpickle
import datetime
import hashlib

class Features:
    TEXT_RANK = 1
    RELATIVE_POS = 2
    LEN = 3
    SIMILARITY = 4
    SENTENCE_RANK = 5
    CONTAINS_ADJECTIVE = 6
    CONTAINS_PROPER_NOUN = 7
    SEARCH_RANK = 8
    SENTENCE_RANK = 9
    IMAGE_URL = 10
    FEATURES_START_IDX = 1
    FEATURES_END_IDX = 9

class Archiver:
    FEATURE_FILE = 'feature_set.pickle'
    FEATURES_UPDATED_FILE = 'feature_set.updated'
    FEATURE_SET_SIZE_KEY = 'feature-set_size'
    
    def __init__(self):
        self.updated = False
        try:
            with open(self.FEATURE_FILE, 'rb') as F:
                self.feature_dict = pickle.load(F)
                if self.FEATURE_SET_SIZE_KEY not in self.feature_dict:
                    self.feature_dict[self.FEATURE_SET_SIZE_KEY] = 6
        except:
            print 'Unpickling error - creating a new feature set'
            self.feature_dict = defaultdict(lambda: defaultdict(lambda: defaultdict(float)))
            self.feature_dict[self.FEATURE_SET_SIZE_KEY] = 0
            
    def get_feature_set_size(self):
        return self.feature_dict[self.FEATURE_SET_SIZE_KEY]
    
    def update_rank(self, session_id, result_id, rank):
        if session_id not in self.feature_dict.keys() or result_id not in self.feature_dict[session_id].keys():
            print 'Error: Could not find a feature-set in archive for session-id %s and result_id %s' % (session_id, str(result_id))
            return
        
        if self.feature_dict[session_id][result_id][Features.SEARCH_RANK] != -1:
            self.feature_dict[self.FEATURE_SET_SIZE_KEY] += 1
            
        self.updated = True
        self.feature_dict[session_id][result_id][Features.SEARCH_RANK] = rank
        
    def add_feature_set(self, session_id, result_id, feature_dict):
        self.updated = True
        self.feature_dict[session_id][result_id] = feature_dict
        
    @staticmethod
    def get_pairwise_diff(v1, v2):
        pairwise_diff = []
        for feature_id in range(Features.FEATURES_START_IDX, Features.FEATURES_END_IDX+1):
            pairwise_diff.append(v1[feature_id] - v2[feature_id])
        
        return pairwise_diff
    
    def get_pairwise_features(self):
        feature_vectors, labels = [], []
        for session_id in self.feature_dict:
            if not isinstance(self.feature_dict[session_id], defaultdict):
                continue
            for result_id1 in self.feature_dict[session_id]:
                for result_id2 in self.feature_dict[session_id]:
                    result1, result2 = self.feature_dict[session_id][result_id1], self.feature_dict[session_id][result_id2] 
                    if result1[Features.SEARCH_RANK] is result2[Features.SEARCH_RANK] \
                    or result1[Features.SEARCH_RANK] is -1 or result2[Features.SEARCH_RANK] is -1:
                        continue
                    
                    feature_vectors.append(self.get_pairwise_diff(result1, result2))
                    labels.append(-1 if result1[Features.SEARCH_RANK] > result2[Features.SEARCH_RANK] else 1)
                
        return (feature_vectors, labels)
    
    def dump(self):
        if self.updated is True:
            ''' Avoid pickling when only feature_sets have been read and not written '''
            with open(self.FEATURE_FILE, 'wb') as F:
                cloudpickle.dump(self.feature_dict, F)
            
            with open(self.FEATURES_UPDATED_FILE, 'w') as F:
                F.write(datetime.datetime.now().isoformat())

    def __del__(self):
        self.dump()

class Extractor:
    MAX_RESULTS = 3
    def __init__(self, text):
        self.feature_set = Archiver()
        self.text = text
        self.results = TextRank.rank_text(text, self.MAX_RESULTS)
        self.session_id = hashlib.md5(text.encode('utf-8')).hexdigest()
    
    def process_results(self):
        result_dict = defaultdict(lambda: defaultdict)
        for idx, res in enumerate(self.results):
            image_result = Search.search_images(res.text, 1)[0]
            feature_dict = defaultdict(float)
            
            feature_dict[Features.IMAGE_URL] = image_result[Search.IMAGE_URL_INDEX]
            #TODO: Should we mark these features differently so that we dont use them in ML?
            feature_dict[Features.SEARCH_RANK] = -1
            feature_dict[Features.LEN] = res.size
            feature_dict[Features.TEXT_RANK] = res.rank
            feature_dict[Features.RELATIVE_POS] = float(res.start_pos) / len(self.text.split())
            feature_dict[Features.SIMILARITY] = Text_Similarity.get_similarity_score(Text_Similarity.get_text_from_url(image_result[Search.SITE_URL_INDEX]), self.text)
            feature_dict[Features.CONTAINS_ADJECTIVE] = count_adjective(res.text)
            feature_dict[Features.CONTAINS_PROPER_NOUN] = count_proper_noun(res.text)
            feature_dict[Features.SENTENCE_RANK] = res.sentence_rank
            
            result_dict[idx] = feature_dict
            self.feature_set.add_feature_set(self.session_id, idx, feature_dict)
            
        return (self.session_id, result_dict) 