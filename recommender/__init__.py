import TextRank
from TextRank import summary
from feature_extractor import Extractor, Features, Archiver
import cjson
from random import random
from classifier import SVM
from utils import ndcg

class Request_Response:
    SESSION_ID_KEY = 'session_id'
    IMAGE_LIST_KEY = 'images'
    IMAGE_RANKS_KEY = 'ranks'
    
    @staticmethod
    def construct_response(display_results, use_ml = False):
        '''
        Sample Response:
        {"images": {"1": (0, "http://www.originlab.com/..."), <rank>: (<result_id>, <image_url>)}, "session_id": "41df44..."}
        '''
        response_dict = {Request_Response.SESSION_ID_KEY: display_results[0]}
        
        image_urls = dict()
        if use_ml is False:
            for res_idx in sorted(display_results[1]):
                image_urls[str(res_idx)] = (res_idx, display_results[1][res_idx][Features.IMAGE_URL])
        else:
            clf = SVM()
            for idx, res_idx in enumerate(sorted(display_results[1], key = lambda x: display_results[1][x], cmp = clf.compare)):
                image_urls[str(idx)] = (res_idx, display_results[1][res_idx][Features.IMAGE_URL])
        
        response_dict[Request_Response.IMAGE_LIST_KEY] = image_urls
        return cjson.encode(response_dict)
    
    @staticmethod
    def parse_feedback(feedback):
        return cjson.decode(feedback)

''' Actual Recommender Code '''
def get_summarised_pictures(text, use_ml = False):
    extractor = Extractor(text)
    results = extractor.process_results()
    return Request_Response.construct_response(results, use_ml)

def expand_training_data(json_response):
    '''
    Expected Response:
    {"ranks": {"1": "0", "8": "7", <result_id>: <feedback_order>}, "session_id": "41df..."}
    '''
    feedback_dict = Request_Response.parse_feedback(json_response)
    trainer = Archiver()
    ranks = []
    for res_id in feedback_dict[Request_Response.IMAGE_RANKS_KEY]:
        ranks.append(feedback_dict[Request_Response.IMAGE_RANKS_KEY][res_id])
        trainer.update_rank(feedback_dict[Request_Response.SESSION_ID_KEY], int(res_id), 
                            int(feedback_dict[Request_Response.IMAGE_RANKS_KEY][res_id]))
    
    with open('ndcg.hist', 'a') as F:
        F.write(str(trainer.get_feature_set_size()) + ':' +  str(ndcg(ranks)))
    
if __name__ == '__main__':
    
    from os import listdir
    from os.path import isfile, join
    ABSTRACT_PATH = '../abstracts'
    for file_name in listdir(ABSTRACT_PATH):
        if not isfile(join(ABSTRACT_PATH, file_name)):
            continue
        
        response = None
        with open(join(ABSTRACT_PATH, file_name)) as F:
            raw_resp = get_summarised_pictures(F.read(), True)
            print raw_resp
            response = cjson.decode(raw_resp)
            
        feedback = {Request_Response.SESSION_ID_KEY: response[Request_Response.SESSION_ID_KEY]}
        
        rankings = dict()
        for image_idx in response[Request_Response.IMAGE_LIST_KEY]:
            rankings[image_idx] = str(int(image_idx) + (-1 if random() < 0.5 else 1))
        feedback[Request_Response.IMAGE_RANKS_KEY] = rankings
        
        expand_training_data(cjson.encode(feedback))
        print cjson.encode(feedback)
        
        (features, labels) = Archiver().get_pairwise_features()
        clf = SVM()
        clf.train()
        
        for idx in range(0, len(features)):
            print clf.predict(features[idx]), labels[idx]