import urllib
import BeautifulSoup
import html2text
import re

from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk import pos_tag
from math import sqrt
import cjson
import requests
from math import log

from time import sleep

def get_all_tags(token_list):
    return set(tuple[1] for tuple in pos_tag(token_list))

def count_adjective(text):
    ans = 0
    for tag in get_all_tags(text.split()):
        if 'J' in tag:
            ans += 1
    
    return ans

def count_proper_noun(text):
    ans = 0
    for tag in get_all_tags(text.split()):
        if 'NN' in tag:
            ans += 1
    
    return ans

def dcg(relevance):
    ret = float(relevance[0])
    for idx in range(1, len(relevance)):
        ret += relevance[idx]/log(1 + idx, 2)
        
    return ret

def ndcg(ranks):
    relevance = [len(ranks) - int(rank) for rank in ranks]
    return dcg(relevance)/dcg(sorted(relevance, reverse = True))
        
class Search:
    SITE_URL_INDEX = 0
    IMAGE_URL_INDEX = 1
    SLEEP_TIME = 5
    
    @staticmethod
    def search_images(query, limit = 10):
        if not len(query):
            print 'Invalid Query'
        URL = 'https://ajax.googleapis.com/ajax/services/search/images?'
        query_terms = urllib.urlencode({'v': '1.0', 'q': query})
        results = None
        while True:
            try:
                url_response = requests.get(URL + query_terms)
                results = cjson.decode(url_response._content)['responseData']['results']
                break
            except Exception, e:
                print 'Received exception while trying to fetch the query results. Sleeping for ' + str(Search.SLEEP_TIME) + ' seconds before retry. Exception: ' + str(e)
                sleep(Search.SLEEP_TIME)
            
        return [(result['originalContextUrl'], result['unescapedUrl']) for result in results][:limit] # Limit to 10 results only


class Text_Similarity:
    @staticmethod
    def is_html_element_visibile(element):
        if element.parent.name in ['style', 'script', '[document]', 'head', 'title']:
            return False
        elif re.match('<!--.*-->', str(element)):
            return False
        return True
 
    @staticmethod   
    def get_text_from_url(url):
        html = urllib.urlopen(url).read()
        res_from_soup, res_from_html2text = None, None
        
        try:
            res_from_html2text = html2text.html2text(html)
        
            soup = BeautifulSoup.BeautifulSoup(html)
            texts = soup.findAll(text=True)
            res_from_soup = ' '.join(filter(Text_Similarity.is_html_element_visibile, texts))
            
            if len(res_from_soup) < len(res_from_html2text):
                return res_from_soup
            
            return res_from_html2text
        
        except Exception, e:
            print 'Exception while parsing HTML contents from url:' + str(url) + '. Exception: ' + str(e)
            if res_from_soup is not None:
                return res_from_soup
            elif res_from_html2text is not None:
                return res_from_html2text
            else:
                return html 
    
    @staticmethod
    def get_token_dict_from_text(text):
        try:
            token_list = Text_Similarity.clean(re.split("[\W]+", text))
            return {token:token_list.count(token) for token in set(token_list)}
        except Exception, e:
            print e
    
    @staticmethod
    def clean(tokens):
        #TODO: Need to cleanup text elements better!   
        stops = stopwords.words('english')
        clean_tokens = [PorterStemmer().stem(token) for token in tokens if token not in stops]
        return clean_tokens
    
    @staticmethod
    def get_vector_magnitude(vector):
        return sqrt(sum([val ** 2 for val in vector.values()]))
    
    @staticmethod
    def get_similarity_score(text1, text2):
        text_dict1 = Text_Similarity.get_token_dict_from_text(text1)
        text_dict2 = Text_Similarity.get_token_dict_from_text(text2)
        
        sim = 0.0
        for key in text_dict1:
            sim += text_dict1[key] * (text_dict2[key] if key in text_dict2 else 0)
        
        return sim / (Text_Similarity.get_vector_magnitude(text_dict2) * Text_Similarity.get_vector_magnitude(text_dict1))
    
if __name__ == '__main__':
#     block1 = ' '.join(open('../abstract', 'r').readlines())
#     block2 = ' '.join(open('../abstract', 'r').readlines())
    if False:
        block1 = Text_Similarity.get_text_from_url('http://en.wikipedia.org/wiki/Learning_to_rank')
        block2 = Text_Similarity.get_text_from_url('http://en.wikipedia.org/wiki/Permutation')
        print Text_Similarity.get_similarity_score(block1, block2)
        
        while True:
            print Search.search_images(raw_input())
    
    print ndcg([5, 4, 3, 2, 1])
    
    