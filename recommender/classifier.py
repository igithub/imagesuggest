import pickle
from cloud.serialization import cloudpickle
from sklearn import svm
from sklearn.preprocessing import normalize 
from os import path, remove as file_remove
from feature_extractor import Archiver

class SVM:
    PICKLED_CLASSIFIER_FILE = 'svm_classifier.pickle'
    
    def __init__(self):
        self.is_updated = False
        try:
            with open(self.PICKLED_CLASSIFIER_FILE, 'rb') as F:
                self.classifier = pickle.load(F)
        except Exception, e:
            print 'Error: Exception unpickling classifier - using a new classifer'
            self.classifier = svm.SVC(kernel = 'linear', C = 0.1)
        
        if path.exists(Archiver.FEATURES_UPDATED_FILE):
            self.train()
    
    def predict(self, feature_vector):
        feature_vector = normalize([feature_vector])[0]
        return self.classifier.predict(feature_vector)
    
    def is_trained(self):
        try:
            return len(self.support_vectors_) > 0
        except:
            pass
        
        return False
    
    def compare(self, fv1, fv2):
        if fv1 == fv2 or not self.is_trained():
            return fv1 < fv2
        return int(self.predict(Archiver.get_pairwise_diff(fv1, fv2))[0])
    
    def train(self):
        if path.exists(Archiver.FEATURES_UPDATED_FILE):
            self.is_updated = True
            (features, labels) = Archiver().get_pairwise_features()
            if len(features) == 0:
                return
            features = normalize(features, axis = 0)
            self.classifier = svm.SVC(kernel = 'linear', C = 0.1).fit(features, labels)
            file_remove(Archiver.FEATURES_UPDATED_FILE)
            
    def __del__(self):
        if self.is_updated is True:
            with open(self.PICKLED_CLASSIFIER_FILE, 'wb') as F:
                cloudpickle.dump(self.classifier, F)                