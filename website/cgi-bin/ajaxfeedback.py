#!/usr/bin/env python

import sys
import json
import cgi
import cgitb
cgitb.enable()

import time

import logging
logging.basicConfig(filename='requests.log', filemode='a', level=logging.DEBUG)

import traceback

try:
    sys.path.append("/var/www/cgi-bin")
    import recommender
    from nltk.tokenize import sent_tokenize
    logging.info("import complete")
except:
    logging.error("import error")
    logging.info(traceback.format_exc())

try:
    logging.info("Fetching values from post")
    fs = cgi.FieldStorage()
    text = fs["text"].value
    text2Log = "Form value " + text
    logging.info(text2Log)
    
    text2Log = "making the request"
    logging.info(text2Log)
    
    recommender.expand_training_data(text)

    sys.stdout.write("Content-Type: application/json\n\n")

    sys.stdout.write("\n")
    sys.stdout.write("\n")

    result = {}
    result['success'] = True
    result['msg'] = "The command completed successfully"
    
    text2Log = "result ready"
    logging.info(text2Log)
    logging.info(json.dumps(result,indent=1))

    sys.stdout.write(json.dumps(result,indent=1))
    sys.stdout.write("\n")

    text2Log = "result sent"
    logging.info(text2Log)

    sys.stdout.close()
except:
    logging.info(traceback.format_exc())

