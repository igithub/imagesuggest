$(document).ready(function(){

	$(document).on('click', '.accept' , function() {
		var id = $(this).attr("id");
		var name = $(this).attr("name");
		var feedback = "{\"ranks\": {\"" + id + "\": \"" + "1" + "\"}, \"session_id\": \"" + name + "\"}";
//		alert(feedback);
		$(this).hide();

		    $.ajax({
			url: "/cgi-bin/ajaxfeedback.py",
			type: "post",
			datatype:"json",
			timeout:300000,
			data: {'text': feedback},
			error: function(xhr, status, error){
			    if(status==="timeout") {
			        alert("Request timed-out!");
			    } else {
				alert(error);
			    }
	    		},
			success: function(response){
			    alert(response.msg);
			},
			complete: function(response){
			   $('.right').text(response.msg);
			}
		    });

	});

	$(document).on('click', '.reject' , function() {
		var id = $(this).attr("id");
		var name = $(this).attr("name");
		var feedback = "{\"ranks\": {\"" + id + "\": \"" + "0" + "\"}, \"session_id\": \"" + name + "\"}";
//		alert(feedback);
		$(this).hide();

		    $.ajax({
			url: "/cgi-bin/ajaxfeedback.py",
			type: "post",
			datatype:"json",
			timeout:300000,
			data: {'text': feedback},
			error: function(xhr, status, error){
			    if(status==="timeout") {
			        alert("Request timed-out!");
			    } else {
				alert(error);
			    }
	    		},
			success: function(response){
			    alert(response.msg);
			},
			complete: function(response){
			   $('.right').text(response.msg);
			}
		    });
	});

	$('.left').change(function(){
	    alert('I\'ve been changed');
	    var text = $('#inputText').val();

	    $.ajax({
		url: "/cgi-bin/ajaxpost.py",
		type: "post",
		datatype:"json",
		timeout:300000,
		data: {'text': text},
		error: function(xhr, status, error){
		    if(status==="timeout") {
	                alert("Request timed-out!");
        	    } else {
			alert(error);
        	    }
    		},
		success: function(response){
		    var properJSONResponse = '[' + response.msg + ']';
//		    alert(properJSONResponse);

		    var allImages = JSON.parse(properJSONResponse);
		    var $rightpane = $('.right');

		    $.each(allImages, function(){
			var session_id = this.session_id;
//			alert(session_id);
			var imagesOnly = JSON.stringify(this.images);
//			alert(imagesOnly);
			var result2 = $.parseJSON(imagesOnly);
			$.each(result2, function(k,v){
		    		var imageURL = "<img src=\"" + v + "\"" + " name=" + session_id + " id=" + k + " width=500 height=500></img>"
				var acceptDiv = "<div class=\"accept\"" + " name=" + session_id + " id=" + k + "><p> <br> Accept </p></div>";
				var rejectDiv = "<div class=\"reject\"" + " name=" + session_id + " id=" + k + "><p> <br> Reject </p></div>";
				//alert(acceptDiv);
				$(imageURL).appendTo('.right');
				$(acceptDiv).appendTo('.right');
				$(rejectDiv).appendTo('.right');
				//$('.accept').click(function(){
				//	alert($(this).attr("id"));
				//});

				//alert(k + ' is ' + v);
			});
		    });
		},
		complete: function(response){
		   $('.right').text(response.msg);
		}
	    });


	});
});
