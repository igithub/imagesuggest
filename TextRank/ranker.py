'''
Created on 23-Mar-2014
@author: gsunder
'''

from utils import LanguageModels as LM
import networkx as NX
from collections import defaultdict
from TextRank.sentence_ranker import SentenceRanker

class summary:
    def __init__(self, start_pos, size, text, rank, sentence_rank = 0):
        self.start_pos = start_pos
        self.size = size
        self.text = text
        self.rank = rank
        self.sentence_rank = sentence_rank
    
    def __str__(self):
        return '%d %d %s %f %f' % (self.start_pos, self.size, self.text, self.rank, self.sentence_rank)

class Ranker:
    CO_OCCURRENCE_WINDOW_SIZE = 7
    ADJECTIVE_INFLUENCE = 0.3
    
    def __init__(self, text):
        # Possible tunable parameters for Text-Ranking could be added here
        self.text = text
        self.sentence_ranker = SentenceRanker(text)
        self.sentence_ranker.rank_sentences()
    
    def rank_text(self, results = 10):
        # Should we keep the stop-words while identifying co-occurrence?
        token_list = LM.tokenize(self.text)
        imp_tokens = LM.extract_important_tokens(token_list)
        imp_token_tuples = [(pos, word) for pos, word in enumerate(token_list) 
                            if word in imp_tokens]
        D = self.construct_graph(imp_token_tuples)
        final_ranks = NX.pagerank(D, max_iter = 200, alpha = 0.85)
        
        positional_word_dict = {}
        for tuple in final_ranks:
            positional_word_dict[tuple[0]] = (tuple[1], final_ranks[tuple])
         
        lexicons = self.construct_lexicons(positional_word_dict)   
        ranked_results = self.get_top_results(lexicons, results)
            
        return ranked_results
    
    def construct_ngrams(self, N, lo, hi, word_dict):
        ans = {}
        for pos in range(lo, hi+1):
            fail = False
            lexicon_parts, combined_rank = [], 0.0
            for idx in range(0, N):
                if pos + idx not in word_dict:
                    fail = True
                    break
                else:
                    '''
                    TODO: Merely adding ranks might not work! Need to add more 
                    semantic meaning into the combined value
                    ''' 
                    lexicon_parts.append(word_dict[pos+idx][0])
                    combined_rank += word_dict[pos+idx][1]
                
            if not fail:
                ans[(pos, N, ' '.join(lexicon_parts))] = combined_rank
        
        return ans
            
    def construct_lexicons(self, positional_word_dict):
        lexicons = defaultdict(float)
        lo, hi = min(positional_word_dict), max(positional_word_dict)
        
        for n in range(1, 4):
            nlexs = self.construct_ngrams(n, lo, hi, positional_word_dict)
            for lex in nlexs:
                lexicons[lex] += nlexs[lex]
                
        return lexicons
    
    def print_summary(self, rank_dict):
        # TODO: Need to remove same positional token repeated as unigram/bigram etc...
        for w in sorted(rank_dict, key=rank_dict.get, reverse=True):
            print w, rank_dict[w]
    
    def get_top_results(self, rank_dict, N):
        pos_vis, token_vis = defaultdict(bool), defaultdict(bool)
        result = []
        for tup in sorted(rank_dict, key=rank_dict.get, reverse=True):
            if token_vis[tup[2]] is True:
                continue
            
            used = False
            for idx in range(0, tup[1]):
                if pos_vis[tup[0] + idx] is True:
                    used = True
                    
            if used is False:
                for idx in range(0, tup[1]):
                    pos_vis[tup[0] + idx] = True
                result.append(summary(tup[0], tup[1], tup[2], rank_dict[tup], self.sentence_ranker.get_token_rank(tup[0])))
                token_vis[tup[2]] = True
                
            if len(result) == N:
                break
        return result
    
    def construct_graph(self, token_tuples):
        graph = NX.DiGraph()
        for i in range(0, len(token_tuples)):
            for j in range(i+1, min(len(token_tuples), i + self.CO_OCCURRENCE_WINDOW_SIZE)):
                ''' 
                TODO: Edge weight could be more dynamic on the following params:
                [1] We could factor in the distance to reduce edge weight between tokens
                [2] Adjectives could have lesser weights assigned! Adjectives lose preference more when distance increases
                [3] Sentence rank and sentence similarity of the tokens' origin can be accounted
                        Sentence Ranker will return ranks and range of each sentence which can be used here.
                [4] We could play with the CO_OCCURRENCE_WINDOW_SIZE
                '''
                
                graph.add_weighted_edges_from([(LM.stem_token(token_tuples[i]), LM.stem_token(token_tuples[j]), self.ADJECTIVE_INFLUENCE / (j-i) * LM.is_adjective(token_tuples[i][1]) + (1 - self.ADJECTIVE_INFLUENCE) / (j-i))])
                
        return graph.to_undirected()
  
if __name__ == '__main__':
    from os import listdir
    from os.path import isfile, join
    ABSTRACT_PATH = '../abstracts'
    for file_name in listdir(ABSTRACT_PATH):
        if not isfile(join(ABSTRACT_PATH, file_name)):
            continue
        with open(join(ABSTRACT_PATH, file_name)) as F:
            text = F.read()
            ranker = Ranker(text.lower())
            print 'Text: %s:' % (text)
            for res in ranker.rank_text():
                print res
            print ''