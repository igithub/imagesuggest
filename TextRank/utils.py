'''
Created on 23-Mar-2014
@author: gsunder
'''

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.stem.porter import PorterStemmer
from nltk import pos_tag
import cjson
import urllib
import requests

class LanguageModels:
    @staticmethod
    def LCS(token1, token2):
        len1, len2 = len(token1), len(token2)
        mem = [[0] * (len2 + 1) for i in range(0, len1 + 1)]
        
        i, j = 0, 0
        try:
            for i in range(1, len1 + 1):
                for j in range(1, len2 + 1):
                    if token1[i-1] == token2[j-1]:
                        mem[i][j] = mem[i-1][j-1] + 1
                    else:
                        mem[i][j] = max(mem[i-1][j], mem[i][j-1])
        except:
            print 'Error'
                    
        return mem[len1][len2]
    
    @staticmethod
    def similarity(token1, token2):
        return LanguageModels.LCS(token1, token2) / float((len(token1) + len(token2)))
        
    @staticmethod
    def tokenize(text):
        # Not removing STOP words (if word not in stopwords.words('english')) to maintain pos_idx
        return [word for sentence in sent_tokenize(text) for word in word_tokenize(sentence)]
    
    @staticmethod
    def is_adjective(token):
        res_tuple = pos_tag([token])
        return 'J' in res_tuple[0][1]
    
    @staticmethod
    def stem_tokens(token_list):
        return [PorterStemmer().stem(token) for token in token_list]
    
    @staticmethod
    def stem_token(token):
        # Should we use stemming? PorterStemmer().stem(token)
        return token
    
    @staticmethod
    def is_token_important(token):
        try:
            tup = pos_tag([token])
            return tup[0][1][0] in ('N', 'A') and tup[0][1] not in ['ADV', 'NUM']
        except:
            return False
    
    @staticmethod
    def extract_important_tokens(token_list):
        return [tup[0] for tup in pos_tag(token_list) if tup[1][0] in ('N', 'A') and tup[1] not in ['ADV', 'NUM']]
    