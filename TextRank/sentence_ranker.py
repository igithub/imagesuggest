'''
Created on 23-Mar-2014
@author: gsunder
'''
from nltk.tokenize import sent_tokenize, word_tokenize
from utils import LanguageModels as LM
import networkx as NX

class sentence:
    def __init__(self, id, start_token, end_token, content):
        self.id = id 
        self.start_token = start_token
        self.end_token = end_token
        # TODO: Can remove this additional information
        self.content = content
        self.rank = 1.0

class SentenceRanker:
    def __init__(self, text):
        self.sentence_list = sent_tokenize(text)
        self.graph = NX.DiGraph()
        self.sentence_info = {}

    def rank_sentences(self):
        cur_token_count = 0
        
        for i in range(0, len(self.sentence_list)):
            token_list_i = word_tokenize(self.sentence_list[i])
            self.sentence_info[i+1] = sentence(i+1, cur_token_count, cur_token_count + len(token_list_i), self.sentence_list[i])
            cur_token_count += len(token_list_i)
            for j in range(i+1, len(self.sentence_list)):
                token_list_j = word_tokenize(self.sentence_list[j])
                similarity = self.find_similarity(token_list_i, token_list_j)
                self.graph.add_weighted_edges_from([(i+1, j+1, similarity)])
                
        self.graph = self.graph.to_undirected()
        self.rank_dict = NX.pagerank(self.graph, max_iter = 200)
        return self.rank_and_display()
        
    def rank_and_display(self, display = False):
        result = {}
        for sentence_id in sorted(self.rank_dict, key = self.rank_dict.get, reverse=True):
            if display is True:
                print self.rank_dict[sentence_id], self.sentence_info[sentence_id].content
            result[self.sentence_info[sentence_id]] = self.rank_dict[sentence_id]
        return result 
    
    def get_token_rank(self, token_pos):
        try:
            for sentence_id in self.sentence_info:
                sent = self.sentence_info[sentence_id]
                if sent.start_token <= token_pos and sent.end_token >= token_pos:
                    return self.rank_dict[sentence_id]
        except:
            return 0
        
        return 0
            
    def find_similarity(self, tlist1, tlist2):
        n1, n2 = len(tlist1), len(tlist2)
        sim = 0.0
        for i in range(0, n1):
            for j in range(0, n2):
                sim += LM.similarity(tlist1[i], tlist2[j])
        
        return sim / float(n1 * n2)
    
if __name__ == '__main__':
    with open('../abstract', 'r') as F:
        ranker = SentenceRanker(F.read())
        ranker.rank_sentences()
        while True:
            x = int(raw_input())
            print ranker.get_token_rank(x)
