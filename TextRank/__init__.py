import ranker, utils
from TextRank.ranker import Ranker, summary

''' Implemented based on the Ranker from http://acl.ldc.upenn.edu/acl2004/emnlp/pdf/Mihalcea.pdf '''
def rank_text_from_file(fd):
    return rank_text(fd.read().lower())

def rank_text(text, max_results = 10):
    return Ranker(text).rank_text(max_results)